﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TaskPlanner
{
    class Serialization<T>
    {
        private string _file;
        public Serialization(string file)
        {
            _file = file;
        }
        public void Serialize(List<T> data)
        {
            File.Delete(_file);
            using (var fs = new FileStream(_file, FileMode.OpenOrCreate))
            {
                var xs = new XmlSerializer(typeof(List<T>));
                xs.Serialize(fs, data);
            }
        }

        public List<T> Deserialize()
        {
            if (File.Exists(_file))
            {
                using (var fs = new FileStream(_file, FileMode.Open))
                {

                    var xs = new XmlSerializer(typeof(List<T>));
                    return (List<T>)xs.Deserialize(fs);
                }
            }
            else
            {
                this.Serialize(new List<T>());
                return new List<T>();
            }
        }
    }
}