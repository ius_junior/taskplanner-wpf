﻿using System;

namespace TaskPlanner.Types
{
    
    class UserTask:Task
    {
        public Guid TaskId { get; set; }
        public string TableNumber { get; set; }
        public string TaskName { get; set; }
        public string TaskInfo { get; set; }
        public string TableStatus { get; set; } // 0 - не начата, 1 - в процессе, 2 - завершена, 3 - просрочена, 4 - приостановлена(таймер)
        public string TableCreated { get; set; }
        public string TableProgress { get; set; }
        public string TableDeadline { get; set; }
        public string TableAccess { get; set; }
        public string TablePlannedOn { get; set; }
        public string TableTimeRating { get; set; }
        public string TableColorStatus { get; set; }
        public string ProjectName { get; set; }
        public string ProjectInfo { get; set; }
        public Guid OwnerId { get; set; }
    }
}
