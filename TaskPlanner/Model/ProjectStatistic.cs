﻿
namespace TaskPlanner.Model
{
    class ProjectStatistic
    {
        public int TaskCount { get; set;}
        public int UserCount { get; set; }
        public int CompletedTasks { get; set; }
        public int TasksInProgress { get; set; }
        public int NotStartedTasks { get; set; }
        public int OverdueTasks { get; set; }
        public string CompletionPercentage { get; set; }

    }
}
