﻿using System;

namespace TaskPlanner
{
    partial class Comment
    {
        public Comment() { }
        public Comment(Guid authorId, Guid taskId, string message)
        {
            Id = Guid.NewGuid();
            Date = DateTime.Now;
            AuthorId = authorId;
            TaskId = taskId;
            Message = message;
        }
    }
}
