﻿using System;

namespace TaskPlanner.Model
{
    class AdminTableTask
    {
        public Guid TaskId { get; set; }
        public Guid ProjectId { get; set; }
        public int Number { get; set; }
        public string TaskName { get; set; }
        public string TaskInfo { get; set; }
        public string Status { get; set; } // 0 - не начата, 1 - в процессе, 2 - завершена, 3 - просрочена
        public string Created { get; set; }
        public string Progress { get; set; }
        public string Deadline { get; set; }
        public string Access { get; set; }
        public string PlannedOn { get; set; }
        public string TimeRating { get; set; }
        public string ProjectName { get; set; }
        public string ProjectInfo { get; set; }
        public Guid OwnerId { get; set; }
    }
}
