﻿using System;

namespace TaskPlanner
{
    partial class Project
    {
        //public Project() { }
        public Project(string name, string info, Guid ownerId)
        {
            Name = name;
            Info = info;
            OwnerId = ownerId;
            Id = Guid.NewGuid();
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
