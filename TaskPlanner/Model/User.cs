﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TaskPlanner
{
    partial class User
    {
        //public User() { }
        public User(string login, string password, string name, string surname, byte rights = 0)
        {
            Id = Guid.NewGuid();
            Login = login;
            Password = password;
            Surname = surname;
            Name = name;
            Rights = rights;
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(Password));
            Password = BitConverter.ToString(checkSum1).Replace("-", String.Empty);
            IsActive = true;
            lastProject = Guid.Empty;
        }
        public override string ToString()
        {
            return string.Format("{0} {1}",Name,Surname);
        }
    }
}
