﻿using System;
using TaskPlanner.CRUD;

namespace TaskPlanner.Services
{
    class DataFactory
    {
        protected static TimeSpan progressParser;
        public static string ProgressParser
        {
            get { return progressParser.ToString(); }
            set { progressParser = TimeSpan.Parse(value); }
        }

        public static void MakeRecords(int count)
        {
            var taskAction = new CrudTask();
            var projectAction = new CrudProject();
            var userAction = new CrudUser();
            var admin = new User("admin", "admin", "AdminName", "AdminSurname", 1);
            userAction.Add(admin);
            userAction.Add(new User("user", "user", "UserName", "UserSurname", 0));
            progressParser = new TimeSpan(0, 0, 0);
            for (int i = 0; i < count; i++)
            {
                var curProject = new Project("Project" + (i + 1), "Project" + (i + 1) + "Info", admin.Id);
                projectAction.Add(curProject);
                for (int j = 0; j < 5; j++)
                {
                    var curUser = new User("User" + (i * 5 + (j + 1)), "user" + (i * 5 + (j + 1)), "User" + (i * 5 + (j + 1)) + "name", "User" + (i * 5 + (j + 1)) + "surname");
                    userAction.Add(curUser);
                    for (int k = 0; k < 5; k++)
                    {
                        taskAction.Add(new Task(curUser.Id, curProject.Id, ((i * 25) + (j * 5) + k + 1), "Task" + ((i * 25) + (j * 5) + k + 1), "Task" + ((i * 25) + (j * 5) + k + 1) + "info", ProgressParser, ProgressParser, DateTime.Today.AddDays(1)));
                    }

                }
            }
        }
    }
}
