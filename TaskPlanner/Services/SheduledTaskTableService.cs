﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.CRUD;
using TaskPlanner.Model;

namespace TaskPlanner.Services
{
    internal class SheduledTaskTableService
    {
        public static IEnumerable<SheduledTableTask> GetTaskTable(Guid id)
        {
            var tasks = new CrudTask().GetAll().Where(t => t.UserId == id);
            return
                tasks.Select(task => new SheduledTableTask()
                {
                    StringPlannedOn = task.PlannedOn.GetValueOrDefault().ToLongDateString(),
                    Id = task.Id,
                    PlannedOn = task.PlannedOn,
                    Access = task.Access,
                    TimeRating = task.TimeRating,
                    Name = task.Name,
                    Deadline = task.Deadline,
                    Info = task.Info,
                    Created = task.Created,
                    Progress = task.Progress,
                    Status = task.Status,
                    ProjectId = task.ProjectId,
                    UserId = task.UserId
                });
        }
    }
}
