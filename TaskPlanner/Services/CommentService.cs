﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.CRUD;
using TaskPlanner.Model;

namespace TaskPlanner.Services
{
    internal class CommentService
    {
        public static IEnumerable<TableComment> GetProjectTable(Guid taskId)
        {
            //var adminTasks = tasks.Where(t => t.UserId == id); //нельзя показывать все таски, нужно выбрать только те что может видеть этот админ
            var comments = new CrudComment().GetAll();
            var userAction = new CrudUser();
            return from comment in comments.Where(c => c.TaskId == taskId)
                   select new TableComment
                   {
                       AuthorId = comment.AuthorId,
                       Id = comment.Id,
                       Message = comment.Message,
                       TaskId = comment.TaskId,
                       Date = comment.Date,
                       DateString = comment.Date.GetValueOrDefault().ToLongDateString() + " " + comment.Date.GetValueOrDefault().ToShortTimeString(),
                       UserName = userAction.Get(comment.AuthorId).ToString(),
                       UserRights = (userAction.Get(comment.AuthorId).Rights == 1) ? "Администратор" : "Пользователь",
                   };
        }
    }
}
