﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.CRUD;
using TaskPlanner.Model;

namespace TaskPlanner.Services
{
    class AdminTableTaskService
    {
        protected static TimeSpan progressParser;
        public static string ProgressParser
        {
            get { return progressParser.ToString(); }
            set { progressParser = TimeSpan.Parse(value); }
        }

        private static string stringFormer(string taskProgress)
        {
            ProgressParser = taskProgress;
            return (((int)progressParser.Hours).ToString() + "ч. " + ((int)progressParser.Minutes).ToString() + "м. " + ((int)progressParser.Seconds).ToString() + "с. ");
        }

        public static IEnumerable<AdminTableTask> GetTaskTable(Guid id,IEnumerable<Task> tasks)
        {
            var statusArray = new string[] { "Не начата", "В процессе", "Завершена", "Просрочена", "Приостановлена" };
            //var adminTasks = tasks.Where(t => t.UserId == id); //нельзя показывать все таски, нужно выбрать только те что может видеть этот админ
            var projects = new CrudProject().GetAll();
            return
                from task in tasks
                join project in projects on task.ProjectId equals project.Id
                select new AdminTableTask()
                {
                    TaskId = task.Id,
                    ProjectId = project.Id,
                    Number = task.Number,
                    TaskName = task.Name,
                    TaskInfo = task.Info,
                    Status = statusArray[task.Status],
                    Created = task.Created.ToLongDateString(),
                    Deadline = task.Deadline.ToLongDateString(),
                    OwnerId = project.OwnerId,
                    Progress = stringFormer(task.Progress), //пофиксить отображение в часах
                    ProjectInfo = project.Info,
                    ProjectName = project.Name,
                    Access = task.Access ? "Открытый" : "Закрытый",
                    TimeRating = stringFormer(task.TimeRating),
                    PlannedOn = task.PlannedOn.GetValueOrDefault().ToLongDateString()
                };

        }
    }
}
