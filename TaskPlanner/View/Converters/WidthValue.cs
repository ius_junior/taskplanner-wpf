﻿using System;
using System.Windows.Data;

namespace TaskPlanner.View.Converters
{
    public class WidthValue : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)

        {

            double s = 300;//(value/2) + 300;

            return (s);

        }

        public object ConvertBack(object value, Type targetTypes, object parameter,
            System.Globalization.CultureInfo culture)

        {

            throw new NotSupportedException("ConvertBack should never be called");

        }

    }

}



