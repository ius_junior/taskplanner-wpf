﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using TaskPlanner.CRUD;
using TaskPlanner.ViewModel;

namespace TaskPlanner
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class UserWindow 
    {
        public UserWindow()
        {
            InitializeComponent();
        }

        private void TabControl_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                ((UserWindowViewModel)this.DataContext).SelectedTask = null;
                PlanneDataGrid.SelectedIndex = -1;
            }
            
        }

    }
}