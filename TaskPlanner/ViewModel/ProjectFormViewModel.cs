﻿using System.Windows.Input;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModel
{
    class ProjectFormViewModel : ViewModelBase
    {
        public string FormName { get; set; }
        public User CurrentUser { get; set; }
        private string _name;
        private string _info;
        private string _nameStar;
        private string _infoStar;
        private string _nameError;
        private string _infoError;
        public ICommand ClickSave { get; set; }
        private Project _dataProject;
        public string Info
        {
            get { return _info; }
            set { _info = value; OnPropertyChanged("Info"); }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        public string NameStar
        {
            get { return _nameStar; }
            set { _nameStar = value; OnPropertyChanged("NameStar"); }
        }
        public string InfoStar
        {
            get { return _infoStar; }
            set { _infoStar = value; OnPropertyChanged("InfoStar"); }
        }
        public string NameError
        {
            get { return _nameError; }
            set { _nameError = value; OnPropertyChanged("NameError"); }
        }
        public string InfoError
        {
            get { return _infoError; }
            set { _infoError = value; OnPropertyChanged("InfoError"); }
        }
        public ProjectFormViewModel(User currentUser)
        {
            CurrentUser = currentUser;
            ClickSave = new Command(arg => SaveProject());
            FormName = Resources.AddNewProject;
        }
        public ProjectFormViewModel(User currentUser, Project dataProject)
            : this(currentUser)
        {
            _dataProject = dataProject;
            Name = dataProject.Name;
            Info = dataProject.Info;
            FormName = Resources.EditProject;
        }
        private void SaveProject()
        {
            int errorCount = 0;
            if (Name == null || Name == "")
            {
                NameStar = "*";
                NameError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Name != null && Name != "")
            {
                NameStar = "";
                NameError = "";
            }
            if (Info == null || Info == "")
            {
                InfoStar = "*";
                InfoError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Info != null && Info != "")
            {
                InfoStar = "";
                InfoError = "";
            }
            if (errorCount > 0)
                return;
            if (_dataProject != null)
            {
                _dataProject.Name = Name;
                _dataProject.Info = Info;
                new CRUD.CrudProject().Update(_dataProject);
            }
            else
            {
                new CRUD.CrudProject().Add(new Project(Name, Info, CurrentUser.Id));
            }
            CloseView = true;
        }
    }
}
