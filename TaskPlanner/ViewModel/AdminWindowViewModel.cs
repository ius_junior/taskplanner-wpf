﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using TaskPlanner.CRUD;
using TaskPlanner.Model;
using TaskPlanner.Properties;
using TaskPlanner.Services;
using TaskPlanner.View;

namespace TaskPlanner.ViewModel
{
    internal class AdminWindowViewModel : ViewModelBase
    {
        public User CurrentUser { get; set; }
        public ICommand ClickAddProject { get; set; }
        public ICommand ClickAddTask { get; set; }
        public ICommand ClickAddUser { get; set; }
        public ICommand ClickChangeUser { get; set; }
        public ICommand ClickSearchUser { get; set; }
        public ICommand ClickSearchTask { get; set; }
        public ICommand ClickSearchProject { get; set; }
        public ICommand ClickDeleteTask { get; set; }
        public ICommand ClickDeleteProject { get; set; }
        public ICommand ClickEditTask { get; set; }
        public ICommand ClickEditUser { get; set; }
        public ICommand ClickEditProject { get; set; }
        public ICommand ClickTaskInfo { get; set; }
        public string SearchProjectText { get; set; }
        public string SearchTaskText { get; set; }
        public string SearchUserText { get; set; }
        private AdminTableUser _selectedUser;
        private AdminTableTask _selectedTask;
        private AdminTableProject _selectedProject;
        private Project _selectedProjectFilter;
        private Project _selectedProjectStat;
        private List<KeyValuePair<string, int>> _projectChartData;

        private ProjectStatistic _currentStatistic;
        private ObservableCollection<AdminTableTask> _tableTasks;
        private ObservableCollection<AdminTableProject> _tableProjects;
        private ObservableCollection<AdminTableUser> _tableUsers;
        private ObservableCollection<Project> _projectsFilter;
        private ObservableCollection<Project> _projectsForStatistics { get; set; }
        private bool _isEditTaskVisible;
        private bool _isEditProjectVisible;
        private bool _isEditUserVisible;
        private bool _isDeleteTaskVisible;
        private bool _isDeleteProjectVisible;


        public List<KeyValuePair<string, int>> ProjectChartData
        {
            get { return _projectChartData; }
            set
            {
                _projectChartData = value;
                this.OnPropertyChanged("ProjectChartData");
            }
        }

        public ProjectStatistic CurrentStatistic
        {
            get { return _currentStatistic; }
            set
            {
                _currentStatistic = value;
                if (value != null)
                {
                    ProjectChartData = new List<KeyValuePair<string, int>>(){
                        new KeyValuePair<string, int>("Выполненные задачи",value.CompletedTasks),
                        new KeyValuePair<string, int>("Не начатые задачи",value.NotStartedTasks),
                        new KeyValuePair<string, int>("Задач в процессе",value.TasksInProgress),
                        new KeyValuePair<string, int>("Просроченные задачи",value.OverdueTasks)
                    };

                }
                this.OnPropertyChanged("CurrentStatistic");
            }
        }

        public Project SelectedProjectStat
        {
            get { return _selectedProjectStat; }
            set
            {
                _selectedProjectStat = value;
                if (value != null) CurrentStatistic = StatisticService.GetProjectStat(value.Id);
                this.OnPropertyChanged("SelectedProjectStat");
            }
        }

        public ObservableCollection<Project> ProjectsForStatistics
        {
            get { return _projectsForStatistics; }
            set
            {
                _projectsForStatistics = value;
                this.OnPropertyChanged("ProjectsForStatistics");
            }
        }

        public Project SelectedProjectFilter
        {
            get { return _selectedProjectFilter; }
            set
            {
                _selectedProjectFilter = value;
                this.OnPropertyChanged("SelectedProject");
                CurrentUser.lastProject = value.Id;
                if (value.Id != Guid.Empty) new CrudUser().Update(CurrentUser);
                ReloadTasks();
            }
        }

        public bool IsEditTaskVisible
        {
            get { return _isEditTaskVisible; }
            set
            {
                _isEditTaskVisible = value;
                this.OnPropertyChanged("IsEditTaskVisible");
            }
        }

        public bool IsEditUserVisible
        {
            get { return _isEditUserVisible; }
            set
            {
                _isEditUserVisible = value;
                this.OnPropertyChanged("IsEditUserVisible");
            }
        }

        public bool IsEditProjectVisible
        {
            get { return _isEditProjectVisible; }
            set
            {
                _isEditProjectVisible = value;
                this.OnPropertyChanged("IsEditProjectVisible");
            }
        }

        public bool IsDeleteTaskVisible
        {
            get { return _isDeleteTaskVisible; }
            set
            {
                _isDeleteTaskVisible = value;
                this.OnPropertyChanged("IsDeleteTaskVisible");
            }
        }

        public bool IsDeleteProjectVisible
        {
            get { return _isDeleteProjectVisible; }
            set
            {
                _isDeleteProjectVisible = value;
                this.OnPropertyChanged("IsDeleteProjectVisible");
            }
        }

        public AdminTableTask SelectedTask
        {
            get { return _selectedTask; }
            set
            {
                _selectedTask = value;
                this.OnPropertyChanged("SelectedTask");
                if (_selectedTask == null)
                {
                    IsDeleteTaskVisible = false;
                    IsEditTaskVisible = false;
                }
                else
                {
                    IsDeleteTaskVisible = true;
                    IsEditTaskVisible = true;
                }
            }
        }

        public AdminTableUser SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                this.OnPropertyChanged("SelectedUser");
                if (_selectedUser == null)
                {
                    IsEditUserVisible = false;
                }
                else
                {
                    IsEditUserVisible = true;
                }
            }
        }

        public AdminTableProject SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                this.OnPropertyChanged("SelectedProject");
                if (_selectedProject == null)
                {
                    IsDeleteProjectVisible = false;
                    IsEditProjectVisible = false;
                }
                else
                {
                    IsDeleteProjectVisible = true;
                    IsEditProjectVisible = true;
                }
            }
        }

        public ObservableCollection<AdminTableProject> TableProjects
        {
            get { return _tableProjects; }
            set
            {
                _tableProjects = value;
                this.OnPropertyChanged("TableProjects");
            }
        }

        public ObservableCollection<AdminTableTask> TableTasks
        {
            get { return _tableTasks; }
            set
            {
                _tableTasks = value;
                this.OnPropertyChanged("TableTasks");
            }
        }

        public ObservableCollection<AdminTableUser> TableUsers
        {
            get { return _tableUsers; }
            set
            {
                _tableUsers = value;
                this.OnPropertyChanged("TableUsers");
            }
        }

        public ObservableCollection<Project> ProjectsFilter
        {
            get { return _projectsFilter; }
            set
            {
                _projectsFilter = value;
                this.OnPropertyChanged("ProjectsFilter");
            }
        }

        public AdminWindowViewModel(User currentUser)
        {
            CurrentUser = currentUser;

            ReloadUsers();
            ReloadProjects();
            ReloadTasks();
            SelectedProjectStat = ProjectsForStatistics.FirstOrDefault();
            ClickSearchProject = new Command(arg => ReloadProjects());
            ClickSearchUser = new Command(arg => ReloadUsers());
            ClickSearchTask = new Command(arg => ReloadTasks());

            ClickAddProject = new Command(arg => AddProject());
            ClickAddUser = new Command(arg => AddUser());
            ClickAddTask = new Command(arg => AddTask());

            ClickEditProject = new Command(arg => EditProject());
            ClickEditUser = new Command(arg => EditUser());
            ClickEditTask = new Command(arg => EditTask());

            ClickDeleteProject = new Command(arg => DeleteProject());
            ClickDeleteTask = new Command(arg => DeleteTask());

            ClickChangeUser = new Command(arg => ChangeUser());
            ClickTaskInfo = new Command(arg => TaskInfo());
            var data = from project in new CrudProject().GetAll().Where(p => p.OwnerId == CurrentUser.Id)
                       group project by project.Id
                           into g
                           select new { Remainder = g.Key, Numbers = g };


            ProjectsFilter = new ObservableCollection<Project>();
            var actionProject = new CRUD.CrudProject();
            foreach (var g in data)
            {
                var t = actionProject.Get(g.Remainder);
                ProjectsFilter.Add(t);
            }
            ProjectsFilter.Insert(0, new Project(Resources.All, "", Guid.Empty) { Id = Guid.Empty });

        }

        private void TaskInfo()
        {
            var UserTask = TaskTableService.GetUserTaskTable(SelectedTask.TaskId);
            var w = new TaskInfo()
            {
                DataContext = new TaskInfoViewModel(CurrentUser, UserTask)
            };
            w.ShowDialog();
        }

        private void AddProject()
        {
            var form = new ProjectForm
            {
                DataContext = new ProjectFormViewModel(CurrentUser)
            };
            form.ShowDialog();
            ReloadProjects();
        }

        private void AddUser()
        {
            var form = new RegistrationForm
            {
                DataContext = new RegistrationFormViewModel(CurrentUser)
            };
            form.ShowDialog();
            ReloadUsers();
        }

        private void AddTask()
        {
            var form = new TaskForm
            {
                DataContext = new TaskFormViewModel(CurrentUser)
            };
            form.ShowDialog();
            ReloadTasks();
        }

        private void ChangeUser()
        {
            var au = new Authorization
            {
                DataContext = new AuthorizationViewModel()
            };
            au.Show();
            CloseView = true;

        }

        private void EditUser()
        {
            if (SelectedUser == null)
                return;
            var form = new RegistrationForm
            {
                DataContext = new RegistrationFormViewModel(CurrentUser, new CRUD.CrudUser().GetAll().FirstOrDefault(u => u.Id == SelectedUser.Id))
            };
            form.ShowDialog();
            ReloadUsers();

        }

        private void EditTask()
        {
            if (SelectedTask == null)
                return;
            var form = new TaskForm
            {
                DataContext = new TaskFormViewModel(CurrentUser, new CRUD.CrudTask().GetAll().FirstOrDefault(t => t.Id == SelectedTask.TaskId))
            };
            form.ShowDialog();
            ReloadTasks();
        }

        private void EditProject()
        {
            if (SelectedProject == null)
                return;
            var form = new ProjectForm
            {
                DataContext = new ProjectFormViewModel(CurrentUser, new CRUD.CrudProject().GetAll().FirstOrDefault(p => p.Id == SelectedProject.ProjectId))
            };
            form.ShowDialog();
            ReloadProjects();
        }

        private void DeleteTask()
        {
            var t = MessageBox.Show(Resources.DeleteTask, Resources.Message, MessageBoxButton.OKCancel);
            if (SelectedTask != null && t.ToString() == "OK")
            {
                new CRUD.CrudTask().Delete(SelectedTask.TaskId);
                TableTasks.Remove(SelectedTask);
            }
            ReloadTasks();
        }

        private void DeleteProject()
        {
            var t = MessageBox.Show(Resources.DeleteProject, Resources.Message, MessageBoxButton.OKCancel);
            if (SelectedProject != null && t.ToString() == "OK")
            {
                new CRUD.CrudProject().Delete(SelectedProject.ProjectId);
                TableProjects.Remove(SelectedProject);
                ReloadProjects();
            }
        }

        private void SearchUser()
        {
            if (SearchUserText != null)
                TableUsers = new ObservableCollection<AdminTableUser>(TableUsers.Where(t => t.Login.ToLower().Contains(SearchUserText.ToLower()) || t.Name.ToLower().Contains(SearchUserText.ToLower()) || t.Surname.ToLower().Contains(SearchUserText.ToLower())).ToList());
        }

        private void SearchTask()
        {
            if (SearchTaskText != null)
                TableTasks = new ObservableCollection<AdminTableTask>(TableTasks.Where(t => t.TaskName.ToLower().Contains(SearchTaskText.ToLower()) || t.Number.ToString().ToLower().Contains(SearchTaskText.ToLower())).ToList());
        }

        private void SearchProject()
        {
            if (SearchProjectText != null)
                TableProjects = new ObservableCollection<AdminTableProject>(TableProjects.Where(t => t.ProjectName.ToLower().Contains(SearchProjectText.ToLower())).ToList());
        }

        private void FilterTasksProject()
        {
            if (_selectedProjectFilter != null && _selectedProjectFilter.Id != Guid.Empty)
                TableTasks = new ObservableCollection<AdminTableTask>(TableTasks.Where(t => t.ProjectId == _selectedProjectFilter.Id));
        }

        private void ReloadProjects()
        {
            TableProjects = new ObservableCollection<AdminTableProject>(AdminTableProjectService.GetProjectTable(CurrentUser.Id, new CRUD.CrudProject().GetAll().OrderBy(p => p.Name)));
            ProjectsForStatistics = new ObservableCollection<Project>(new CrudProject().GetAll().OrderBy(p => p.Name));
            SearchProject();
        }

        private void ReloadTasks()
        {
            TableTasks = new ObservableCollection<AdminTableTask>(AdminTableTaskService.GetTaskTable(CurrentUser.Id, new CRUD.CrudTask().GetAll().OrderBy(p => p.Number)));
            FilterTasksProject();
            SearchTask();
        }

        private void ReloadUsers()
        {
            TableUsers =
                new ObservableCollection<AdminTableUser>(AdminTableUserService.GetUserTable(CurrentUser.Id,
                    new CRUD.CrudUser().GetAll()));
            SearchUser();
        }
    }
}
