﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModel
{
    class TaskFormViewModel : ViewModelBase
    {
        public User CurrentUser { get; set; }
        public string FormName { get; set; }
        public DateTime _timeRating { get; set; }
        private Task _dataTask;
        private string _name;
        private string _info;
        private string _selectedUserStar;
        private string _selectedUserError;
        private string _selectedProjectStar;
        private string _selectedProjectError;
        private string _taskNameStar;
        private string _taskNameError;
        private string _timeRatingStar;
        private string _timeRatingError;
        private string _desTaskStar;
        private string _desTaskError;
        public ICommand ClickSave { get; set; }
        private DateTime _deadline;
        private ObservableCollection<User> _users;
        private ObservableCollection<Project> _projects;
        private User _selectedUser;
        private Project _selectedProject;
        public DateTime TimeRating
        {
            get { return _timeRating; }
            set { _timeRating = value; OnPropertyChanged("TimeRating"); }
        }
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set { _selectedProject = value; OnPropertyChanged("SelectedProject"); }
        }
        public User SelectedUser
        {
            get { return _selectedUser; }
            set { _selectedUser = value; OnPropertyChanged("SelectedUser"); }
        }
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { _users = value; OnPropertyChanged("Users"); }
        }
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set { _projects = value; OnPropertyChanged("Projects"); }
        }
        public DateTime Deadline
        {
            get { return _deadline; }
            set { _deadline = value; OnPropertyChanged("Deadline"); }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        public string Info
        {
            get { return _info; }
            set { _info = value; OnPropertyChanged("Info"); }
        }
        public string SelectedUserStar
        {
            get { return _selectedUserStar; }
            set { _selectedUserStar = value; OnPropertyChanged("SelectedUserStar"); }
        }
        public string SelectedUserError
        {
            get { return _selectedUserError; }
            set { _selectedUserError = value; OnPropertyChanged("SelectedUserError"); }
        }
        public string SelectedProjectStar
        {
            get { return _selectedProjectStar; }
            set { _selectedProjectStar = value; OnPropertyChanged("SelectedProjectStar"); }
        }
        public string SelectedProjectError
        {
            get { return _selectedProjectError; }
            set { _selectedProjectError = value; OnPropertyChanged("SelectedProjectError"); }
        }
        public string TaskNameStar
        {
            get { return _taskNameStar; }
            set { _taskNameStar = value; OnPropertyChanged("TaskNameStar"); }
        }
        public string TaskNameError
        {
            get { return _taskNameError; }
            set { _taskNameError = value; OnPropertyChanged("TaskNameError"); }
        }
        public string TimeRatingStar
        {
            get { return _timeRatingStar; }
            set { _timeRatingStar = value; OnPropertyChanged("TimeRatingStar"); }
        }
        public string TimeRatingError
        {
            get { return _timeRatingError; }
            set { _timeRatingError = value; OnPropertyChanged("TimeRatingError"); }
        }
        public string DesTaskStar
        {
            get { return _desTaskStar; }
            set { _desTaskStar = value; OnPropertyChanged("DesTaskStar"); }
        }
        public string DesTaskError
        {
            get { return _desTaskError; }
            set { _desTaskError = value; OnPropertyChanged("DesTaskError"); }
        }
        public TaskFormViewModel(User currentUser)
        {
            CurrentUser = currentUser;

            var userList = new CRUD.CrudUser().GetAll().Where(u => u.Rights == 0 && u.IsActive == true);
            Users = new ObservableCollection<User>(userList);

            var projectList = new CRUD.CrudProject().GetAll();
            Projects = new ObservableCollection<Project>(projectList);

            FormName = Resources.CreateTask;
            Deadline = DateTime.Now;
            ClickSave = new Command(arg => SaveTask());
        }
        public TaskFormViewModel(User currentUser, Task dataTask)
            : this(currentUser)
        {
            _dataTask = dataTask;
            Deadline = dataTask.Deadline;

            var userList = new CRUD.CrudUser().GetAll().Where(u => u.Rights == 0 && (u.IsActive == true || u.Id == dataTask.UserId));
            Users = new ObservableCollection<User>(userList);

            SelectedUser = Users.First(u => u.Id == dataTask.UserId);
            SelectedProject = Projects.First(u => u.Id == dataTask.ProjectId);
            Name = dataTask.Name;
            Info = dataTask.Info;
            FormName = Resources.EditTask;
            TimeRating = Convert.ToDateTime(dataTask.TimeRating);
        }
        private void SaveTask()
        {
            int errorCount = 0;
            if (SelectedUser == null)
            {
                SelectedUserStar = "*";
                SelectedUserError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (SelectedUser != null)
            {
                SelectedUserStar = "";
                SelectedUserError = "";
            }
            if (SelectedProject == null)
            {
                SelectedProjectStar = "*";
                SelectedProjectError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (SelectedProject != null)
            {
                SelectedProjectStar = "";
                SelectedProjectError = "";
            }
            if (Name == null || Name == "")
            {
                TaskNameStar = "*";
                TaskNameError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Name != null && Name != "")
            {
                TaskNameStar = "";
                TaskNameError = "";
            }
            if (TimeRating.Hour == 0)
            {
                TimeRatingStar = "*";
                TimeRatingError = "Значение часов не должно быть равным 0";
                errorCount++;
            }
            if (TimeRating.Hour != 0)
            {
                TimeRatingStar = "";
                TimeRatingError = "";
            }
            if (Info == null || Info == "")
            {
                DesTaskStar = "*";
                DesTaskError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Info != null && Info != "")
            {
                DesTaskStar = "";
                DesTaskError = "";
            }
            if (errorCount > 0)
                return;
            if (_dataTask != null)
            {
                _dataTask.Name = Name;
                _dataTask.Info = Info;
                _dataTask.Deadline = Deadline;
                _dataTask.ProjectId = SelectedProject.Id;
                _dataTask.UserId = SelectedUser.Id;
                _dataTask.TimeRating = new TimeSpan(TimeRating.Hour, TimeRating.Minute, 0).ToString();
                _dataTask.Progress = new TimeSpan(0, 0, 0).ToString();
                new CRUD.CrudTask().Update(_dataTask);
            }
            else
            {
                new CRUD.CrudTask().Add(new Task(SelectedUser.Id, SelectedProject.Id, 0, Name, Info, new TimeSpan(0, 0, 0).ToString(), new TimeSpan(TimeRating.Hour, TimeRating.Minute, 0).ToString(), Deadline));
            }
            CloseView = true;
        }
    }
}
