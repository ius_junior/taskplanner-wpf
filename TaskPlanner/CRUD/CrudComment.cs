﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace TaskPlanner.CRUD
{
    class CrudComment
    {
        private PlannerBaseEntities Context;
        public CrudComment()
        {
            Context=new PlannerBaseEntities();
        }

        /// <summary>
        /// Получить все комменты
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IEnumerable<Comment> GetAll()
        {
            return Context.Comments;
        }

        /// <summary>
        /// Получить комментарий по Id
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
         public Comment Get(Guid guid)
        {
            return Context.Comments.FirstOrDefault(l => l.Id == guid);
        }

         /// <summary>
         /// Получить несколько комментов по Id
         /// </summary>
         /// <param name="guidList"></param>
         /// <returns></returns>
         public IEnumerable<Comment> GetMany(List<Guid> guidCommentList)
         {
             List<Comment> commentList = new List<Comment>();
             foreach (var item in guidCommentList)
                 commentList.Add(Context.Comments.FirstOrDefault(l => l.Id == item));
             return commentList;
         }

        /// <summary>
        /// Добавить комментарий
        /// </summary>
        /// <param name="record"></param>
         public bool Add(Comment record)
         {
             Context.Comments.Add(record);
            Context.SaveChanges();
             return true;
         }

        /// <summary>
        /// Удалить коментарий по Id
        /// </summary>
        /// <param name="guid"></param>
        public void Delete(Guid guid)
        {
            Context.Comments.Remove(Context.Comments.FirstOrDefault(r => r.Id == guid));
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить коментарий
        /// </summary>
        /// <param name="record"></param>
        public void Update(Comment record)
        {
            Context.Comments.AddOrUpdate(record);
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить несколько коментариев
        /// </summary>
        /// <param name="recordList"></param>
        public void UpdateMany(List<Comment> recordList)
        {
            foreach (var item in recordList)
            {
              Context.Comments.AddOrUpdate(item);
            }
            Context.SaveChanges();
        }
    }
}
