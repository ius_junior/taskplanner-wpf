﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace TaskPlanner.CRUD
{
    class CrudTask
    {
        private PlannerBaseEntities Context;
        public CrudTask()
        {
            Context = new PlannerBaseEntities();
        }

        /// <summary>
        /// Получить все задачи
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAll()
        {
            return Context.Tasks;
        }

        /// <summary>
        /// Получить задачу по Id
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Task Get(Guid guid)
        {
            return Context.Tasks.FirstOrDefault(l => l.Id == guid);
        }

        /// <summary>
        /// Получить несколько задач по Id
        /// </summary>
        /// <param name="guidList"></param>
        /// <returns></returns>
        public List<Task> GetMany(IEnumerable<Guid> guidTaskList)
        {
            List<Task> taskList = new List<Task>();
            foreach (var item in guidTaskList)
                taskList.Add(Context.Tasks.FirstOrDefault(l => l.Id == item));
            return taskList;
        }

        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="record"></param>
        public bool Add(Task record)
        {
            if (Context.Tasks.FirstOrDefault(u => u.Name == record.Name) != null)
                return false;
            record.Number = Context.Tasks.Count() + 1;
            Context.Tasks.Add(record);
            Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Удалить задачу по Id
        /// </summary>
        /// <param name="guid"></param>
        public void Delete(Guid guid)
        {
            Context.Tasks.Remove(Context.Tasks.FirstOrDefault(r => r.Id == guid));
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить задачу
        /// </summary>
        /// <param name="record"></param>
        public void Update(Task record)
        {
            Context.Tasks.AddOrUpdate(record);
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить несколько задач
        /// </summary>
        /// <param name="recordList"></param>
        public void UpdateMany(List<Task> recordList)
        {
            foreach (var item in recordList)
            {
                Context.Tasks.AddOrUpdate(item);
            }
            Context.SaveChanges();
        }
    }
}
