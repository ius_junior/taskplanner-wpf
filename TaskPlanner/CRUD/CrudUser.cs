﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace TaskPlanner.CRUD
{
    class CrudUser
    {
        private PlannerBaseEntities Context;
        public CrudUser()
        {
            Context = new PlannerBaseEntities();
        }
        /// <summary>
        /// Получить всех пользователей
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IEnumerable<User> GetAll()
        {
            return Context.Users;

        }

        /// <summary>
        /// Получить пользователя по Id
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public User Get(Guid guid)
        {

            return Context.Users.FirstOrDefault(l => l.Id == guid);
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="record"></param>
        public bool Add(User record)
        {

            if (Context.Users.FirstOrDefault(u => u.Login == record.Login) != null)
            {
                return false;
            }
            Context.Users.Add(record);
            Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Удалить пользователя по Id
        /// </summary>
        /// <param name="guid"></param>
        public void Delete(Guid guid)
        {
            var user = Context.Users.FirstOrDefault(r => r.Id == guid);
            var tasks = new CrudTask().GetAll().Where(t => t.UserId == guid);
            foreach (var task in tasks)
            {
                task.UserId = Guid.Empty;
            }

            Context.Users.Remove(user);
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить пользователя
        /// </summary>
        /// <param name="record"></param>
        public void Update(User record)
        {
            Context.Users.AddOrUpdate(record);
            Context.SaveChanges();
        }

    }
}
